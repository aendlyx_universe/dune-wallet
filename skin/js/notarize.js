const chunk_size = 1024 * 1024;

function hash_file(input_id, comments_id, name_id) {
  return new Promise((resolve, reject) => {
    let files = document.getElementById(input_id).files;
    if (!files.length) {
      reject('Please select a file!');
    }
    let comments = document.getElementById(comments_id).value;
    let name = document.getElementById(name_id).innerHTML;
    let str = (comments == "") ? name : name + "__" + comments;
    let a = new TextEncoder().encode(str);
    var meta_data = sjcl.codec.hex.fromBits(sjcl.codec.bytes.toBits(a));
    let file = files[0],
        reader = new FileReader(),
        sha256 = new sjcl.hash.sha256(),
        start = 0,
        end = chunk_size,
        ichunk = 0;
    reader.onloadend = function () {
      let a = new Uint8Array(reader.result);
      let data = sjcl.codec.bytes.toBits(a);
      sha256.update(data);
      if (end >= file.size) {
        let hctx = sha256.finalize();
        let res = sjcl.codec.hex.fromBits(hctx);
        resolve({hash: res, meta_data: meta_data})
      } else {
        start = end;
        end = end + chunk_size;
        let blob = file.slice(start, end);
        reader.readAsArrayBuffer(blob);
      }
    };
    reader.onerror = () => {
      reject("error")
    };
    let blob = file.slice(start, end);
    reader.readAsArrayBuffer(blob);
  })
};

function clear_inputs(input_id, comments_id, name_id) {
  document.getElementById(comments_id).value = "";
  document.getElementById(input_id).value = "";
  document.getElementById(name_id).innerHTML = "";
}
