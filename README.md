# Dune Wallet
This project is a fork of Tezbox web wallet: [official repository](https://github.com/tezbox/desktop-wallet)

The official url of the Dune Wallet is https://wallet.dune.network


### How to build build eztz.min.js

```
git clone git@gitlab.com:dune-network/ezdun.git
cd ezdun
npm install
npm run-script build
```
